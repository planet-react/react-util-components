function zeroOad( number ) {
    return number < 10 ? '0' + number : String( number )
}

function getDayNames( sundayFirst = false, locale = 'en-US' ) {
    let result = [];
    let d = new Date();
    for ( let i = 0; i < 7; i++ ) {
        let day = sundayFirst ? i : i + 1;
        d.setDate( d.getDate() - d.getDay() + day );
        result.push( {
            short: d.toLocaleString( locale, { weekday: 'short' } ),
            long: d.toLocaleString( locale, { weekday: 'long' } )
        } )
    }
    return result;
}

function getMonthNames( locale = 'en-US' ) {
    let result = [];
    let d = new Date();
    for ( let i = 0; i < 12; i++ ) {
        d.setMonth( i );
        result.push( {
            index: i,
            short: d.toLocaleString( locale, { month: 'short' } ),
            long: d.toLocaleString( locale, { month: 'long' } )
        } )
    }
    return result;
}

function getCalendar( year, month, selectedDate ) {
    let firstDay = new Date( year, month, 1 );
    let lastDay = new Date( year, month + 1, 0 );
    let firstDayOfWeek = firstDay.getDay();
    let days = [];

    for ( let i = firstDay.getDate(); i < firstDayOfWeek; i++ ) {
        days.push( null );
    }

    for ( let i = firstDay.getDate(); i < lastDay.getDate() + 1; i++ ) {
        days.push( i );
    }

    const selected = selectedDate ? selectedDate : new Date();

    //@todo: add today

    return {
        year: year,
        month: month,
        daysInMonth: days,
        firstDay: firstDay.getDate(),
        lastDay: lastDay.getDate(),
        selectedDay: selected.getDate(),
        selectedMonth: selected.getMonth(),
        selectedYear: selected.getFullYear()
    };
}

function getHours( steps = [ 30 ] ) {
    let result = [];
    let h = 12;
    let period = 'AM';
    for ( let i = 0; i < 24; i++ ) {
        if ( h == 13 ) {
            h = 1;
        }

        if ( i == 12 ) {
            period = 'PM';
        }

        result.push( h + ':00 ' + period );

        for ( let j = 0; j < steps.length; j++ ) {
            result.push( h + ':' + steps[ j ] + ' ' + period );
        }

        h++;
    }
    return result;
}

/**
 * Format:
 *
 *  M - 1, 2, ..., 12
 *  Mo - 1st, 2nd, ..., 12th
 *  MM - 01, 02, ..., 12
 *  MMM - Jan, Feb, ..., Dec
 *  MMMM - January, February, ..., December
 */
function formatMonth( monthData, format ) {

    let formatted = null;

    const index = monthData.index + 1;

    switch ( format ) {
        case 'M':
            formatted = index;
            break;
        case 'Mo':
            //@todo
            formatted = index;
            break;
        case 'MM':
            formatted = zeroOad( index );
            break;
        case 'MMM':
            formatted = monthData.short;
            break;
        case 'MMMM':
            formatted = monthData.long;
            break;
        default:
            formatted = monthData.long
    }

    return formatted;
}

export {
    getDayNames,
    getCalendar,
    getMonthNames,
    getHours,
    formatMonth
}