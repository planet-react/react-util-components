import React, { Component } from "react";
import PropTypes from "prop-types";
import { getMonthNames, formatMonth } from '../util';

export default class MonthList extends Component {

    static propTypes = {
        value: PropTypes.number,
        onSelect: PropTypes.func,
        locale: PropTypes.string,
        disableFromIndex: PropTypes.number,
        displayFormat: PropTypes.oneOf( [ 'M', 'MM', 'MMM', 'MMMM' ] ),
        className: PropTypes.string,
        style: PropTypes.object
    };

    static defaultProps = {
        locale: 'en-US',
        className: 'pr-month-list',
        displayFormat: 'MMMM'
    };

    months = [];

    constructor( props ) {
        super( props );
        this.months = getMonthNames( props.locale );
    }

    onSelect = ( event ) => {
        const { month } = event.target.dataset;
        if ( !month ) {
            return;
        }

        const disabled = event.target.dataset.disabled == 'true';
        const value = Number( month );
        const { short, long } = this.months[ value ];
        if ( this.props.onSelect ) {
            this.props.onSelect( {
                locale: this.props.locale,
                value: value,
                short,
                long,
                disabled
            } );
        }
    };

    render() {
        const { value, displayFormat, disableFromIndex, className, style } = this.props;

        return (
            <ul className={className} style={style} onClick={this.onSelect}>
                {this.months.map( ( month, i ) => {
                    const disabled = disableFromIndex != undefined && i > disableFromIndex;
                    let classes = [ 'pr-month-list__item' ];
                    if ( i == value ) {
                        classes.push( 'pr-month-list__item--selected' );
                    }
                    if ( disabled ) {
                        classes.push( 'pr-month-list__item--disabled' );
                    }
                    classes = classes.join( ' ' );

                    return (
                        <li data-month={i}
                            data-disabled={disabled}
                            className={classes}
                            key={i}>
                            {formatMonth( month, displayFormat )}
                        </li>)
                } )}
            </ul>
        )
    }
}
