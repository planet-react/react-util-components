import React, { Component } from "react";
import PropTypes from "prop-types";
import { getHours } from '../util'
export default class TimeList extends Component {

    static propTypes = {
        value: PropTypes.string,
        onSelect: PropTypes.func,
        steps: PropTypes.array,
        className: PropTypes.string,
        style: PropTypes.object,
    };

    static defaultProps = {
        steps: [ 30 ],
        className: 'pr-time-list',
    };

    constructor( props ) {
        super( props );
        this.state = {
            value: props.value
        };
    }

    componentWillReceiveProps( nextProps ) {
        this.setState( {
            value: nextProps.value
        } );
    }

    onSelect = ( event ) => {
        const value = event.target.dataset.time;
        if ( this.props.onSelect ) {
            this.props.onSelect( value );
        }
    };

    render() {
        const { value } = this.state;
        const { className, steps, style } = this.props;
        const hours = getHours( steps );

        return (
            <ul onClick={this.onSelect}
                style={style}
                className={className}>
                {hours.map( ( hour, i ) => {
                    let selected = value != '' && value === hour;
                    let classes = 'pr-time-list__item' + (selected ? ' pr-time-list__item--selected' : '');
                    return (
                        <li data-time={hour}
                            className={classes}
                            data-selected={selected} key={i}>
                            {hour}
                        </li>
                    );
                } )}
            </ul>
        )
    }
}
