import React from 'react';
import PropTypes from 'prop-types';

CalendarHeader.propTypes = {
    onPreviousMonth: PropTypes.func.isRequired,
    onNextMonth: PropTypes.func.isRequired,
    year: PropTypes.number.isRequired,
    monthName: PropTypes.string.isRequired,
    className: PropTypes.string,
    headerTitleClass: PropTypes.string,
    headerButtonClass: PropTypes.string,
};

export default function CalendarHeader( { onPreviousMonth, onNextMonth, year, monthName, className, headerTitleClass, headerButtonClass } ) {
    return (
        <div className="pr-calendar__header">
            <a className="pr-calendar__navigation-button"
               onClick={onPreviousMonth}>
                ❮
            </a>
            <div className="pr-calendar__title">{monthName} {year}</div>
            <a className="pr-calendar__navigation-button"
               onClick={onNextMonth}>
                ❯
            </a>
        </div>
    );
};

