import React from 'react';
import PropTypes from 'prop-types';

DayNames.propTypes = {
    dayNames: PropTypes.array.isRequired,
    className: PropTypes.string,
    dayNameClass: PropTypes.string,
};

export default function DayNames( { dayNames, className, dayNameClass } ) {
    return (
        <ul className="pr-calendar__day-names">
            {dayNames.map( ( name, i ) => {
                return (
                    <li className="pr-calendar__day-name"
                        key={i}>
                        {name.short}
                    </li>
                )
            } )}
        </ul>
    );
};

