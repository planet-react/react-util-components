import React, { Component } from 'react';
import PropTypes from "prop-types";
import Header from './Header';
import DayNames from './DayNames';
import Days from './Days';
import {
    getMonthNames,
    getDayNames,
    getCalendar
} from '../util';

export default class Calendar extends Component {

    static propTypes = {
        startDate: PropTypes.object,
        onSelect: PropTypes.func,
        className: PropTypes.string,
        style: PropTypes.object
    };

    static defaultProps = {
        className: 'pr-calendar'
    };

    constructor( props ) {
        super( props );
        const { startDate } = props;
        const d = startDate ? startDate : new Date();
        this.state = {
            year: d.getFullYear(),
            month: d.getMonth()
        };
    }

    componentWillReceiveProps( nextProps ) {
        this.setDateMonth( nextProps.startDate );
    }

    setDateMonth( date ) {
        const d = date ? date : new Date();
        this.setState( {
            year: d.getFullYear(),
            month: d.getMonth()
        } );
    }

    onPreviousMonth = () => {
        const { year, month } = this.state;
        const date = new Date( year, month, 1 );
        date.setMonth( date.getMonth() - 1 );

        this.setDateMonth( date );
    };

    onNextMonth = () => {
        const { year, month } = this.state;
        const date = new Date( year, month, 1 );
        date.setMonth( date.getMonth() + 1 );
        this.setDateMonth( date );
    };

    onSelect = ( event ) => {
        if ( !event.target.dataset.day ) {
            return;
        }
        if ( this.props.onSelect ) {
            const { year, month } = this.state;
            const day = Number( event.target.dataset.day );
            // const result = formatDate( new Date( year, month + 1, day ), this.props.format );
            this.props.onSelect( new Date( year, month, day ) );
        }
    };

    render() {
        const { year, month } = this.state;
        const { startDate, className, style } = this.props;
        const calendar = getCalendar( year, month, startDate );
        const dayNames = getDayNames();
        const monthName = getMonthNames()[ month ].short;

        return (
            <div className={className} style={style}>
                <Header onPreviousMonth={this.onPreviousMonth}
                        onNextMonth={this.onNextMonth}
                        year={year}
                        monthName={monthName}/>

                <DayNames dayNames={dayNames}/>

                <Days calendar={calendar} onSelect={this.onSelect}/>
            </div>
        )
    }
}
