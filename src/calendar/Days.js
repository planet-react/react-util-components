import React from 'react';
import PropTypes from 'prop-types';

Days.propTypes = {
    calendar: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
    className: PropTypes.string,
    dayClass: PropTypes.string,
    selectedClass: PropTypes.string,
    todayClass: PropTypes.string,
};

export default function Days( { calendar, onSelect, className, dayClass, selectedClass, todayClass } ) {
    const { year, month, selectedYear, selectedMonth, daysInMonth, selectedDay } = calendar;
    const today = new Date();

    return (
        <ul className="pr-calendar__days">
            {daysInMonth.map( ( day, i ) => {
                const isSelected = year == selectedYear && month == selectedMonth && day == selectedDay;
                const isToday = year == today.getFullYear() && month == today.getMonth() && day == today.getDate();
                const classNames = 'pr-calendar__day' +
                    (isSelected ? ' pr-calendar__day--selected' : '') +
                    (isToday ? ' pr-calendar__day--today' : '');

                return (
                    <li className={classNames}
                        key={i}
                        data-day={day}
                        onClick={onSelect}>
                        {day}
                    </li>
                )
            } )}
        </ul>
    );
};

