const path = require( 'path' );
const webpack = require( 'webpack' );

module.exports = {
    entry: [
        './src/index'
    ],
    output: {
        path: path.join( __dirname, 'lib' ),
        filename: 'index.js',
        libraryTarget: 'umd'
    },
    plugins: [
        new webpack.DefinePlugin( {
            'process.env': {
                'NODE_ENV': JSON.stringify( 'production' )
            }
        } ),
        new webpack.optimize.UglifyJsPlugin( {
            compress: {
                warnings: false
            }
        } ),
    ],
    module: {
        loaders: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }
        ]
    }
};