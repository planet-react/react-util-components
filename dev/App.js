import React, { Component } from 'react';
import { Calendar, MonthList, TimeList } from '../src';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/styles';

const Output = ( { str } ) => (<div>Selected: {str}</div>);

const Container = ( { isVisible, children } ) => (
    <div style={{ display: isVisible ? '' : 'none' }}>{children}</div>
);

const Highlight = ( { children } ) => {
    const codeString = '(num) => num + 1';
    return (
        <SyntaxHighlighter
            style={docco}
            wrapLines={true}
            lineStyle={lineNumber => {
                return { display: 'block', whiteSpace: 'pre' };
            }}
        >
            {children}
        </SyntaxHighlighter>
    );
};

const CALENDAR_PROPS = `startDate: PropTypes.object,
onSelect: PropTypes.func,
className: PropTypes.string,
style: PropTypes.object`;

const MONTHLIST_PROPS = `value: PropTypes.number,
onSelect: PropTypes.func,
locale: PropTypes.string,
displayFormat: PropTypes.oneOf( [ 'M', 'Mo', 'MM', 'MMM', 'MMMM' ] ),
className: PropTypes.string,
style: PropTypes.object
`;

const TIMELIST_PROPS = `value: PropTypes.string.isRequired,
onSelect: PropTypes.func.isRequired,
steps: PropTypes.array,
className: PropTypes.string,
style: PropTypes.object,`;

export default class App extends Component {

    state = {
        visibleContainer: '',
        monthListMonth: null,
        timeListTime: '',
        calendarDate: new Date(),
    };

    constructor( props ) {
        super( props );
    }

    onMonthListSelect = ( data ) => {
        console.log(data);
        this.setState( { monthListMonth: data.value } )
    };

    onTimeListSelect = ( timeStr ) => {
        this.setState( { timeListTime: timeStr } )
    };

    onCalendarSelect = ( dateObj ) => {
        this.setState( { calendarDate: dateObj } )
    };

    toggleContainer = ( container ) => {
        const { visibleContainer } = this.state;
        this.setState( {
            visibleContainer: visibleContainer === container ? '' : container
        } );
    };

    render() {
        const { visibleContainer } = this.state;

        return (
            <div>
                <h2 onClick={this.toggleContainer.bind( this, 'calendar' )}>
                    Calendar
                </h2>
                <Container isVisible={visibleContainer === 'calendar'}>
                    { this.state.calendarDate &&
                    <Output
                        str={this.state.calendarDate.toISOString().split( 'T' )[ 0 ]}/> }

                    <Calendar startDate={this.state.calendarDate}
                              onSelect={this.onCalendarSelect}/>

                    <Highlight>
                        {CALENDAR_PROPS}
                    </Highlight>

                </Container>

                <h2 onClick={this.toggleContainer.bind( this, 'monthList' )}>
                    MonthList</h2>
                <Container isVisible={visibleContainer === 'monthList'}>
                    <Output str={this.state.monthListMonth}/>
                    <MonthList value={this.state.monthListMonth}
                               disableFromIndex={5}
                               onSelect={this.onMonthListSelect}/>

                    <Highlight>
                        {MONTHLIST_PROPS}
                    </Highlight>
                </Container>

                <h2 onClick={this.toggleContainer.bind( this, 'timeList' )}>
                    TimeList
                </h2>
                <Container isVisible={visibleContainer === 'timeList'}>
                    <Output str={this.state.timeListTime}/>
                    <TimeList value={this.state.timeListTime}
                              onSelect={this.onTimeListSelect }/>
                    <Highlight>
                        {TIMELIST_PROPS}
                    </Highlight>
                </Container>

            </div>
        )
    }
}