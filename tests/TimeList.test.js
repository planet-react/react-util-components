import React from 'react';
import {TimeList} from '../src';
import renderer from 'react-test-renderer';
const MockDate = require('mockdate');

it( 'renders correctly', () => {

    //MockDate.set(1434319925275);

    const tree = renderer.create(
        <TimeList/>
    ).toJSON();

    expect( tree ).toMatchSnapshot();

    //MockDate.reset();
} );