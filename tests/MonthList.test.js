import React from 'react';
import { MonthList } from '../src';
import renderer from 'react-test-renderer';
// const MockDate = require( 'mockdate' );

it( 'renders correctly', () => {

    //MockDate.set(1434319925275);

    const tree = renderer.create(
        <MonthList/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    //MockDate.reset();
} );

it( 'renders with given displayFormat prop', () => {
    let tree = renderer.create(
        <MonthList displayFormat="M"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <MonthList displayFormat="MM"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <MonthList displayFormat="MMM"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <MonthList displayFormat="MMMM"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

} );